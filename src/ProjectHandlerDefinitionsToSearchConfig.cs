﻿using System.Configuration;
using MessageHandler;

namespace ProjectHandlerDefinitionsToSearch
{
    /// <summary>
    /// Configuration section for projection
    /// </summary>
    [HandlerConfiguration]
    public class ProjectHandlerDefinitionsToSearchConfig : ConfigurationSection
    {
        /// <summary>
        /// A connection string to storage
        /// </summary>
        [ConfigurationProperty("ConnectionString", IsRequired = false)]
        public string ConnectionString
        {
            get { return this["ConnectionString"] as string; }
            set { this["ConnectionString"] = value; }
        }

        /// <summary>
        /// An identifier for the source type, used to build up the projection
        /// </summary>
        [ConfigurationProperty("SourceType", IsRequired = false)]
        public string SourceType
        {
            get { return this["SourceType"] as string; }
            set { this["SourceType"] = value; }
        }

        /// <summary>
        /// The name of the search service to project to
        /// </summary>
        [ConfigurationProperty("SearchServiceName", IsRequired = false)]
        public string SearchServiceName
        {
            get { return this["SearchServiceName"] as string; }
            set { this["SearchServiceName"] = value; }
        }

        /// <summary>
        /// The api key of the search service to project to
        /// </summary>
        [ConfigurationProperty("SearchServiceApiKey", IsRequired = false)]
        public string SearchServiceApiKey
        {
            get { return this["SearchServiceApiKey"] as string; }
            set { this["SearchServiceApiKey"] = value; }
        }

        /// <summary>
        /// The index in the search service to project to
        /// </summary>
        [ConfigurationProperty("SearchServiceIndex", IsRequired = false)]
        public string SearchServiceIndex
        {
            get { return this["SearchServiceIndex"] as string; }
            set { this["SearchServiceIndex"] = value; }
        }
    }
}
