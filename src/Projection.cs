using MessageHandler;
using MessageHandler.SDK.EventSource;
using MessageHandler.Contracts.HandlerDefinitions;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace ProjectHandlerDefinitionsToSearch
{
    public class Projection :
        IProjection<SearchEntry, HandlerDefinitionAddedToCategory>,
        IProjection<SearchEntry, HandlerDefinitionCreated>,
        IProjection<SearchEntry, HandlerDefinitionLoaded>,
        IProjection<SearchEntry, HandlerDefinitionRenamed>,
        IProjection<SearchEntry, HandlerDefinitionVersionAccepted>,
        IProjection<SearchEntry, HandlerDefinitionVersionActivated>,
        IProjection<SearchEntry, HandlerDefinitionVersionAdded>,
        IProjection<SearchEntry, HandlerDefinitionRemovedFromCategory>,
        IProjection<SearchEntry, HandlerDefinitionVersionApprovalRequested>,
        IProjection<SearchEntry, HandlerDefinitionVersionRejected>,
        IProjection<SearchEntry, HandlerDefinitionConfigurationAdded>,
        IProjection<SearchEntry, HandlerDefinitionAddedToProject>,
        IProjection<SearchEntry, HandlerDefinitionSourceProviderAuthorized>,
        IProjection<SearchEntry, HandlerDefinitionRepositoryAssociated>,
        IProjection<SearchEntry, HandlerDefinitionRepositoryDisassociated>,
        IProjection<SearchEntry, HandlerDefinitionOwnershipTransfered>,
        IProjection<SearchEntry, HandlerDefinitionBinariesProviderAssociated>,
        IProjection<SearchEntry, HandlerDefinitionBinariesProviderEnabled>,
        IProjection<SearchEntry, HandlerDefinitionBinariesProviderDisabled>,
        IProjection<SearchEntry, HandlerDefinitionBinariesProviderDisassociated>,
        IProjection<SearchEntry, HandlerDefinitionObsoleted>
    {
        public void ProjectInternal(SearchEntry record, dynamic msg)
        {
            var categories = ((IList<string>)msg.Details.Categories)?.Where(c => !string.IsNullOrEmpty(c)).ToArray();
            record.Id = msg.Details.Identifier;
            record.Title = msg.Details.Name;
            record.Description = msg.Details.Description;
            record.Account = msg.Details.Owner;
            record.Object = Json.Encode(msg.Details);
            record.Filter1 = msg.Details.ActiveVersion?.Version;
            record.Reference1 = msg.Details.Project;
            record.Collection1 = categories != null && categories.Length > 0 ? categories : new string[0];
            record.Bool1 = msg.Details.Obsolete;
            record.Type = "HandlerDefinition";
        }

        public void Project(SearchEntry record, HandlerDefinitionLoaded msg)
        {
            ProjectInternal(record, msg);
        }

        public void Project(SearchEntry record, HandlerDefinitionCreated msg)
        {
            ProjectInternal(record, msg);
        }

        public void Project(SearchEntry record, HandlerDefinitionAddedToCategory msg)
        {
            ProjectInternal(record, msg);
        }

        public void Project(SearchEntry record, HandlerDefinitionRenamed msg)
        {
            ProjectInternal(record, msg);
        }

        public void Project(SearchEntry record, HandlerDefinitionVersionAccepted msg)
        {
            ProjectInternal(record, msg);
        }

        public void Project(SearchEntry record, HandlerDefinitionVersionActivated msg)
        {
            ProjectInternal(record, msg);
        }

        public void Project(SearchEntry record, HandlerDefinitionVersionAdded msg)
        {
            ProjectInternal(record, msg);
        }

        public void Project(SearchEntry record, HandlerDefinitionRemovedFromCategory msg)
        {
            ProjectInternal(record, msg);
        }

        public void Project(SearchEntry record, HandlerDefinitionVersionApprovalRequested msg)
        {
            ProjectInternal(record, msg);
        }

        public void Project(SearchEntry record, HandlerDefinitionVersionRejected msg)
        {
            ProjectInternal(record, msg);
        }

        public void Project(SearchEntry record, HandlerDefinitionConfigurationAdded msg)
        {
            ProjectInternal(record, msg);
        }

        public void Project(SearchEntry record, HandlerDefinitionAddedToProject msg)
        {
            ProjectInternal(record, msg);
        }

        public void Project(SearchEntry record, HandlerDefinitionSourceProviderAuthorized msg)
        {
            ProjectInternal(record, msg);
        }

        public void Project(SearchEntry record, HandlerDefinitionRepositoryAssociated msg)
        {
            ProjectInternal(record, msg);
        }

        public void Project(SearchEntry record, HandlerDefinitionRepositoryDisassociated msg)
        {
            ProjectInternal(record, msg);
        }

        public void Project(SearchEntry record, HandlerDefinitionOwnershipTransfered msg)
        {
            ProjectInternal(record, msg);
        }

        public void Project(SearchEntry record, HandlerDefinitionBinariesProviderAssociated msg)
        {
            ProjectInternal(record, msg);
        }

        public void Project(SearchEntry record, HandlerDefinitionBinariesProviderDisassociated msg)
        {
            ProjectInternal(record, msg);
        }

        public void Project(SearchEntry record, HandlerDefinitionBinariesProviderDisabled msg)
        {
            ProjectInternal(record, msg);
        }

        public void Project(SearchEntry record, HandlerDefinitionBinariesProviderEnabled msg)
        {
            ProjectInternal(record, msg);
        }

        public void Project(SearchEntry record, HandlerDefinitionObsoleted msg)
        {
            ProjectInternal(record, msg);
        }
    }
}