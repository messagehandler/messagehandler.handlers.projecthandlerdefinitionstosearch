using MessageHandler;
using MessageHandler.SDK.EventSource;

namespace ProjectHandlerDefinitionsToSearch
{
    public class AddToContainer : IInitialization
    {
        public void Init(IContainer container)
        {
            var source = container.Resolve<IConfigurationSource>();
            var config = source.GetConfiguration<ProjectHandlerDefinitionsToSearchConfig>();
            container.UseEventSourcing(config.ConnectionString)
                    .EnableProjections(config.ConnectionString);
        }
    }
}