﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reactive.Linq;
using System.Threading.Tasks;
using MessageHandler;
using MessageHandler.SDK.EventSource;
using Yaus.Framework.AzureSearch;
using Environment = MessageHandler.Environment;

namespace ProjectHandlerDefinitionsToSearch
{
    public class ProjectHandlerDefinitionsToSearchHandler :
        IStandingQuery<DynamicJsonObject>,
        IAction<DynamicJsonObject>
    {
        private readonly AzureSearchClient _searchclient;
        private readonly string _sourceType;
        private readonly string _index;
        private readonly SynchronizedCollection<SearchEntry> _pending = new SynchronizedCollection<SearchEntry>();
        private readonly IInvokeProjections _projectionInvoker;

        public ProjectHandlerDefinitionsToSearchHandler(IConfigurationSource source, IVariablesSource variables, ITemplatingEngine templating, IInvokeProjections projectionInvoker)
        {
            _projectionInvoker = projectionInvoker;

            dynamic channelVariables = variables.GetChannelVariables(Channel.Current());
            dynamic accountVariables = variables.GetAccountVariables(Account.Current());
            dynamic environmentVariables = variables.GetEnvironmentVariables(Environment.Current());

            var config = source.GetConfiguration<ProjectHandlerDefinitionsToSearchConfig>();

            var serviceName = templating.Apply(config.SearchServiceName, null, channelVariables, environmentVariables, accountVariables);
            var apiKey = templating.Apply(config.SearchServiceApiKey, null, channelVariables, environmentVariables, accountVariables);
            _sourceType = templating.Apply(config.SourceType, null, channelVariables, environmentVariables, accountVariables);
            _index = templating.Apply(config.SearchServiceIndex, null, channelVariables, environmentVariables, accountVariables);

            _searchclient = new AzureSearchClient(serviceName, apiKey);
        }

        public IObservable<DynamicJsonObject> Compose(IObservable<DynamicJsonObject> messages)
        {
            return from e in messages
                   where IsHandlerDefinitionEvent(e)
                   select e;
        }

        private bool IsHandlerDefinitionEvent(dynamic e)
        {
            string what = e.What;
            string handlerDefinitionId = e.HandlerDefinitionId;
            string sourceId = e.SourceId;
            string sourceType = e.SourceType;

            Trace.TraceInformation("Evaluating What:'{0}', HandlerDefinitionId:'{1}', SourceId:'{2}', SourceType:'{3}'", what, handlerDefinitionId, sourceId, sourceType);

            return e.What != null && e.HandlerDefinitionId != null && e.SourceId != null && e.SourceType == _sourceType;
        }

        public async Task Action(DynamicJsonObject t)
        {
            dynamic m = t;

            string handlerDefinitionId = m.HandlerDefinitionId;
            string what = m.What;

            Trace.TraceInformation("Received event from challenge '{0}'.", handlerDefinitionId);

            var eventType = GetType(what);
            if (eventType == null)
            {
                Trace.TraceInformation("Received event of unknown type '{0}' skipping", what);
                return;
            }

            var @event = Json.Decode(Json.Encode(ToDictionaryRecursive(t)), eventType);
            if (@event == null)
            {
                Trace.TraceInformation("Could not deserialize json into claimed type '{0}'", what);
                return;
            }

            var definition = _pending.FirstOrDefault(r => r.Id == handlerDefinitionId) 
                                ?? _searchclient.Get<SearchEntry>(_index, handlerDefinitionId).Result 
                                ?? new SearchEntry() { Id = handlerDefinitionId };

            Trace.TraceInformation("Retrieved handler definition '{0}', going to apply projection.", handlerDefinitionId);

            _projectionInvoker.Invoke(definition, @event);

            if (!_pending.Contains(definition))
            {
                _pending.Add(definition);
            }

            Trace.TraceInformation("Projection applied to handler definition '{0}', persisting.", handlerDefinitionId);
        }

        private static Type GetType(string typeName)
        {
            var type = Type.GetType(typeName);
            if (type != null) return type;
            foreach (var a in AppDomain.CurrentDomain.GetAssemblies())
            {
                if (a.IsDynamic) continue;

                type = a.ExportedTypes.FirstOrDefault(t => t.Name == typeName);
                if (type != null)
                    return type;
            }
            return null;
        }

        public async Task Complete()
        {
            foreach (var entry in _pending)
            {
                await _searchclient.Upsert(_index, new object[] { entry });
            }

            _pending.Clear();
        }

        private static IDictionary<string, object> ToDictionaryRecursive(DynamicJsonObject decode)
        {
            var dict = decode.ToDictionary();
            var result = new Dictionary<string, object>();
            foreach (var key in dict.Keys)
            {
                var jo = dict[key] as DynamicJsonObject;
                if (jo != null)
                {
                    result[key] = ToDictionaryRecursive(jo);
                }
                else if (dict[key] is DynamicJsonArray)
                {
                    var list = (from o in (DynamicJsonArray)dict[key]
                                let djo = o as DynamicJsonObject
                                select djo != null ? ToDictionaryRecursive(djo) : o).ToList();
                    result[key] = list;
                }
                else
                {
                    result[key] = dict[key];
                }
            }

            return result;
        }

    }
}